---
order: 70
---
# Captchas

Some endpoints may require captchas to proceed, in order to slow down automated malicious traffic.
Legitimate users might also be affected, based on the frequency of write requests or due certain endpoints being particularly sensitive to malicious use, such as user signup.

Once an endpoint decides that a captcha needs to be solved, a 403 Forbidden response will be returned, with the error code `captcha_required_exception`.
The sitekey needed for recaptcha to function is provided in both the `X-Captcha-Sitekey` header field, as well as in the error context, specified as `siteKey` parameter.

The captcha result of the client can either be passed into the repeated original request with the `X-Captcha-Result` header or alternatively to the `POST /captcha/solve` endpoint.
The time a solved captcha is remembered varies across different endpoints and can also be influenced by individual client behavior.

Authentication is not required for the `POST /captcha/solve` endpoint, captchas are tracked both by client ip and logged in user id.
If you are logged in, you want to send the session token along, so you validate the captcha for your client ip and user id at the same time, but it is not required.
