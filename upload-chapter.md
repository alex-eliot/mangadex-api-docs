---
order: 51
---
# Chapter Upload

## In A Nutshell

To upload a chapter, you need to start an upload-session, upload files to this session and once done, commit the session with a ChapterDraft.
Uploaded Chapters will generally be put into a queue for staff approval and image processing before it is available to the public.

## Limits

- 1 Active Upload Session per user. Before opening a second session, either commit or abandon your current session
- 10 files per one PUT request is max
- 500 files per upload session is max
- 20 MB max uploaded session filesize
- 150 MB max total sum of all uploaded session filesizes
- Allowed file extensions: jpg, jpeg, png, gif
- Image must fit into max resolution of 10000x10000 px

## Example

You need to be logged in for any upload operation. Which Manga you're allowed to upload to and which contributing Scanlation Groups you're free to credit depend on your individual user state.

To start an upload session, we pick the manga-id we want to upload to and the group-ids we have to credit. We use the official test manga `f9c33607-9180-4ba6-b85c-e4b5faee7192` and the group "Unknown" with id `145f9110-0a6c-4b71-8737-6acb1a3c5da4`. If no group can be credited, we would not send any group-id at all, otherwise we can credit up to 5 groups.
Note that crediting all involved groups is mandatory, doing otherwise will lead to a rejection of your upload.

The first step is optional, but because only one upload session is allowed per user, we check if we have any open upload sessions by doing `GET /upload`. We expect a 404 response with error detail 'No upload session found'.

Next step is to begin our upload session. We send a `POST /upload/begin` with json data. (If you want to edit an existing chapter, append the chapter id after it `POST /upload/begin/db99d333-76e9-4e66-9c97-4831c43ac96c` with its version as the json payload)

Request:
```json
{
  'manga' => 'f9c33607-9180-4ba6-b85c-e4b5faee7192',
  'groups': [
    '145f9110-0a6c-4b71-8737-6acb1a3c5da4'
  ]
}
```

Response:
```json
{
  'result': 'ok',
  'data': {
    'id': '113b7724-dcc2-4fbc-968f-9d775fcb1cd6',
    'type': 'upload_session',
    'attributes': {
      'isCommitted': false,
      'isProcessed': false,
      'isDeleted': false
    },
    'relationships': [
      {
        'id': '41ce3e1a-8325-45b5-af8e-06aaf648a0df',
        'type': 'user'
      },
      {
        'id': 'f9c33607-9180-4ba6-b85c-e4b5faee7192',
        'type': 'manga'
      },
      {
        'id': '145f9110-0a6c-4b71-8737-6acb1a3c5da4',
        'type': 'scanlation_group'
      }
    ]
  }
}
```

the `data.id` is what you want to store because you will need it for the following steps. We will refer to it as the `uploadSessionId` from here on out.

Remember the `GET /upload` request from the beginning? Try it again and you will see that it will return the same uploadSessionId. You can only have one upload session per user until you commit or abandon it, which makes it easy for you to continue uploading at a later time.

Now that we have a `uploadSessionId`, we can upload images. Any .jpg, .jpeg, .png or .gif files are fine, archives like .zip, .cbz or .rar are not. You will have to extract those archives beforehand if you want to make this work.

For each file, send a POST request like `POST /upload/{uploadSessionId}` with the image data. FormData seems to work best with `Content-Type: multipart/form-data; boundary=boundary`, mileage might vary depending on your programming language. Join our discord and ask for advice if in doubt.

You can upload a number of files in a single request (currently max. 10). The response body will be successful (response.result == 'ok') but might also contain errors if one or more files failed to validate. It's up to you to handle bad uploads and retry or reupload as you see fit. Successful uploads will be returned in the data array as type `upload_session_file`

A successful response could look like this:
```json
{
  'result': 'ok',
  'errors': [],
  'data': [
    {
      'id': '12cc211a-c3c3-4f64-8493-f26f9b98c6f6',
      'type': 'upload_session_file',
      'attributes': {
        'originalFileName': 'testimage1.png',
        'fileHash': 'bbf9b9548ee4605c388acb09e8ca83f625e5ff8e241f315eab5291ebd8049c6f',
        'fileSize': 18920,
        'mimeType': 'image/png',
        'version': 1
      }
    }
  ]
}
```
Store the data[{index}].id attribute as the `uploadSessionFileId`, this will be the unique identifier for the file you just uploaded.

If you change your mind and want to remove a previously uploaded image, you can send a request like `DELETE /upload/{uploadSessionId}/{uploadSessionFileId}`, expecting a response like
```json
{
  'response': 'ok'
}
```

Finally you can commit your upload session. We opened with a manga-id and group-ids but what we actually want is to upload a chapter. For that we have to build a payload consisting of two things: a chapterDraft and a pageOrder. The payload will look similar to the following:

```json
{
  'chapterDraft': {
    'volume': '1',
    'chapter': '2.5',
    'title': 'Read Online',
    'translatedLanguage': 'en'
  },
  'pageOrder': [
      '12cc211a-c3c3-4f64-8493-f26f9b98c6f6'
  ]
}
```

the `chapterDraft` is the chapter data you would like to create, the pageOrder is an ordered list of uploadSessionFileIds you uploaded earlier.

Order didnt matter, now it does. Any files you uploaded but do not specify in this pageOrder array will be deleted.

An example response is:
```json
{
  'result': 'ok',
  'data': {
    'id': '14d4639b-5a8f-4f42-a277-b222412930ca',
    'type': 'chapter',
    'attributes': {
      'volume': '1',
      'chapter': '2.5',
      'title': 'Read Online',
      'translatedLanguage': 'en',
      'publishAt': null,
      'createdAt': '2021-06-16T00:40:22+00:00',
      'updatedAt': '2021-06-16T00:40:22+00:00',
      'version': 1
    },
    'relationships': [
      {
        'id': '145f9110-0a6c-4b71-8737-6acb1a3c5da4',
        'type': 'scanlation_group'
      },
      {
        'id': 'f9c33607-9180-4ba6-b85c-e4b5faee7192',
        'type': 'manga'
      },
      {
        'id': '41ce3e1a-8325-45b5-af8e-06aaf648a0df',
        'type': 'user'
      }
    ]
  }
}
```

You just uploaded a chapter. Congratz!

The returned chapter has empty data and dataSaver attributes where otherwise the pages would be. This is because the image processing happens asynchroniously. Depending on how many chapters need to be processed at a given time, it might take a while for this to be updated.

The first time you upload a chapter in a language you didn't upload before, it will have to be approved by staff.
Until both imageprocessing and possible approval have happened, your chapter will be held back and not appear on the website or be found in the list and search endpoints.

As mentioned in the beginning, to edit a chapter use the `POST /upload/begin/{chapterId}` endpoint [`begin-edit-session`](https://api.mangadex.org/swagger.html#/Upload/begin-edit-session) with the current chapter version as the json POST-body payload, and the UploadSession will come pre-filled with the remote existing UploadSessionFiles which you can reorder, remove, upload new images and commit your changes afterward as if it was a new chapter.
