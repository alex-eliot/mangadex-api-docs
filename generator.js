'use strict'

const yaml = require('js-yaml')
const fs = require('fs')

const doc = yaml.load(fs.readFileSync('./static/api.yaml', 'utf8'))

const genStatusCode = status => {
  if (status >= 200 && status < 400) {
    return `<span style="color: green">${status}</span>`
  } else if (status >= 400) {
    return `<span style="color: red">${status}</span>`
  } else {
    return `<span style="color: white">${status}</span>`
  }
}

const requiredTagModel = '<span class="requiredValue">required</span>'

// TODO: Refactor to remove styles from tag and onto the css file
const makeDetailsTag = (summary, details, tabIndex, savePath) => {
  const contentCss = [
    `margin: 0 0 0 ${tabIndex * 2}rem`
  ]

  if (savePath) {
    const ret = `<details>` +
                  `<summary>` +
                    `${summary}` +
                  `</summary>` +
                `</details>`
    fs.writeFileSync(savePath, `<div style="${contentCss.join('; ')}">${details}</div>`)
    return ret
  } else {
    const ret = `<details>` +
                  `<summary>` +
                    `${summary}` +
                  `</summary>` +
                  `${details ? `<div style="${contentCss.join('; ')}">${details}</div>` : ''}` +
                `</details>`
    return ret
  }
}

const makeCopyButton = pathToCopy => {
  const attrs = [
    'onClick="copyPath(event)"',
    'onmouseenter="showCopyHint(event)"',
    'onmouseleave="hideCopyHint(event)"'
  ]

  return (
    `<button ${attrs.join(' ')} value="${pathToCopy}">` +
      `<img class="done" src="/docs/static/svg/done.svg">` +
      `<img class="copy" src="/docs/static/svg/copy.svg">` +
    `</button>`
  )
}

const makeResponseItem = (content, pathToCopy, detailsPath) => {
  const attrs = ['onmouseenter="showCopyBtn(event)"', 'onmouseleave="hideCopyBtn(event)"']

  if (detailsPath) {
    attrs.push(`onClick=genDetails(event)`, `value="/docs/static/${detailsPath.toLowerCase()}.html"`)
  }

  const css = [`margin: auto 0`]

  return (
    `<div ${attrs.join(' ')} class="responseItem">` +
      `<p style="${css.join('; ')}" class="responseKey">${content}</p>` +
      `<div class="copyBtnAndHintContainer">` +
        `<span class="copyHint hintHidden">Copy path</span>` +
        `<span class="copyButton hidden">${makeCopyButton(pathToCopy)}</span>` +
      `</div>` +
    `</div>`
  )
}

const makePathItem = path => {
  return `<span class="path_to_endpoint">${path}</span>`
}

const genTypeStringFromParam = (schema, tabIndex, source, optionalRef) => {
  let type
  const attributes = []

  if (optionalRef) {
    return recursiveSchemaBuilder(optionalRef, tabIndex, '')
  }

  if (schema.description) {
    attributes.push(`description: ${schema.description}`)
  }

  if (schema.oneOf) {
    const index = tabIndex ?? 2

    for (var i = 0; i < schema.oneOf.length; i++) {
      attributes.push(
        makeDetailsTag(
          `Case ${i + 1}`,
          genTypeStringFromParam(schema.oneOf[i], index + 1, `${source}/oneOf[${i}]`, null),
          index
        )
      )
    }

    type = makeDetailsTag(`Dynamic`, attributes.join('<br>'), 1)

    return type
  }

  if (schema.type) {
    switch (schema.type) {
      case 'array': {
        Object.keys(schema.items).forEach(key => {
          switch (key) {
            case 'enum': {
              attributes.push(makeDetailsTag(`Enum`, schema.items.enum.join('<br>'), tabIndex))
              break
            }

            case '$ref': {
              type = genTypeStringFromParam(null, tabIndex, null, schema.items['$ref'].slice(2))
              break
            }

            default: {
              attributes.push(`${key}: ${schema.items[key]}`)
              break
            }
          }
        })

        if (attributes.length > 0) {
          type = makeDetailsTag(`Array`, attributes.join('<br>'), 1)
        } else {
          type = 'Array'
        }

        break
      }

      case 'integer': {
        const min = schema.minimum
        const max = schema.maximum

        if (typeof min != 'undefined') {
          attributes.push(`minimum: ${min}`)
        }

        if (typeof max != 'undefined') {
          attributes.push(`maximum: ${max}`)
        }

        if (attributes.length > 0) {
          type = makeDetailsTag(`Integer`, attributes.join('<br>'), 1)
        } else {
          type = 'Integer'
        }

        break
      }

      case 'string': {
        if (schema.enum) {
          attributes.push(makeDetailsTag('Enum', schema.enum.join('<br>'), tabIndex))
        }

        if (schema.format) {
          attributes.push(`format: ${schema.format}`)
        }

        if (attributes.length > 0) {
          type = makeDetailsTag(`String`, attributes.join('<br>'), 1)
        } else {
          type = 'String'
        }

        break
      }

      case 'object': {
        if (schema.properties) {
          type = makeDetailsTag(
            'Object',
            genTypeStringFromParam(null, tabIndex - 1, `${source}/properties`, `${source}/properties`),
            tabIndex
          )
        } else {
          if (attributes.length > 0) {
            type = makeDetailsTag('Object', attributes.join('<br>'), 1)
          } else {
            type = 'Object'
          }
        }

        break
      }
    }
  }

  return type
}

const makePathTable = (items, source) => {
  const markdown = []

  for (var i = 0; i < items.length; i++) {
    const q = items[i]

    const name = q.name
    const type = genTypeStringFromParam(q.schema, 1, `${source}[${i}]`)?.replaceAll(/\|/g, '\\|')

    const requiredTag = q.required ? requiredTagModel : ''

    markdown.push(`| ${name} ${requiredTag} | ${type} |`)
  }

  return markdown
}

const makeHeaderTable = items => {
  const markdown = []

  items.forEach(q => {
    const name = q.name
    const value = q.schema.default

    if (q.required) {
      markdown.push(`| ${name} ${requiredTagModel} | ${value}`)
    } else {
      markdown.push(`| ${name} | ${value} `)
    }
  })

  return markdown
}

const makeQueryTable = (items, source) => {
  const markdown = []

  for (var i = 0; i < items.length; i++) {
    const q = items[i]

    let name

    if (q.schema?.default) {
      name = makeDetailsTag(q.name, `default: ${JSON.stringify(q.schema.default)}`, 1)
    } else {
      name = q.name
    }

    const type = genTypeStringFromParam(q.schema, 1, `${source}[${i}]`)?.replaceAll(/\|/g, '\\|')

    markdown.push(`| ${name} | ${type} |`)
  }

  return markdown
}

const makeReqBodyTable = (ref, required) => {
  const markdown = []

  let component = doc

  let dirs = ref.split('/')

  let i
  let limit = dirs.length

  for (i = 0; i < limit; i++) {
    if (dirs[i].at(-1) === '~') {
      dirs = [...dirs.slice(0, i), dirs[i].slice(0, -1) + '/' + dirs[i + 1], ...dirs.slice(i + 2)]
      i -= 1
      limit--
    } else if (dirs[i].at(0) === '~') {
      dirs = [...dirs.slice(0, i - 1), dirs[i - 1] + '/' + dirs[i].slice(1), ...dirs.slice(i + 1)]
      i -= 2
      limit--
    }
  }

  dirs.forEach(key => {
    if (key.match(/\[([0-9]*)\]$/)) {
      const index = key.match(/\[([0-9]*)\]$/)[1]
      component = component[key.slice(0, -(index.length + 2))][parseInt(index)]
      if (component.schema) {
        component = component.schema
      }
    } else {
      component = component[key]
    }
  })

  Object.keys(component).forEach(key => {
    const isRequired = required?.indexOf(key) != -1
    const requiredTag = isRequired ? requiredTagModel : ''

    const attributes = []

    switch (key) {
      case '$ref': {
        const newRef = component['$ref'].slice(2)
        markdown.push(...makeReqBodyTable(newRef, required))
      }

      case 'title': {
        if (dirs.at(-1) != 'properties') {
          break
        }

        if (component.title['$ref']) {
          const newRef = component.title['$ref'].slice(2)

          markdown.push(
            `| ${key} ${requiredTag} | ${makeDetailsTag(
              newRef.split('/').at(-1),
              recursiveSchemaBuilder(newRef, 1),
              1
            )?.replaceAll(/\|/g, '\\|')}`
          )
          break
        }

        break
      }

      case 'properties': {
        markdown.push(...makeReqBodyTable(`${ref}/properties`, required))

        break
      }

      case 'additionalProperties': {
        if (!component[key]) {
          break
        }

        Object.keys(component[key]).forEach(attr => {
          switch (attr) {
            default: {
              attributes.push(`${attr}: ${component[key][attr]}`)

              break
            }
          }
        })

        if (attributes.length > 0) {
          markdown.push(
            `${dirs.at(-1)} ${requiredTag} | ${makeDetailsTag(`Object`, attributes.join('<br>'), 1)?.replaceAll(
              /\|/g,
              '\\|'
            )}`
          )
        } else {
          markdown.push(`${dirs.at(-1)} ${requiredTag} | Object`)
        }

        break
      }

      case 'allOf': {
        let required
        component.allOf.forEach(comp => {
          if (comp.required) {
            required = comp.required
          }
        })

        component.allOf.forEach(comp => {
          if (comp['$ref']) {
            const newRef = comp['$ref'].slice(2)
            markdown.push(...makeReqBodyTable(newRef, required))
          }
        })

        break
      }

      case 'type': {
        let shouldBreak = true
        switch (component.type) {
          case 'object': {
            break
          }

          default:
            if (typeof component.type === 'object') {
              shouldBreak = false;
            }
            break
        }

        if (shouldBreak) {
          break
        }
      }

      default: {
        switch (component[key].type) {
          case 'string': {
            Object.keys(component[key]).forEach(attr => {
              switch (attr) {
                case 'enum': {
                  attributes.push(makeDetailsTag(`Enum`, component[key][attr].join('<br>'), 1))

                  break
                }

                case 'type': {
                  break
                }

                default: {
                  attributes.push(`${attr}: ${component[key][attr]}`)

                  break
                }
              }
            })

            if (attributes.length > 0) {
              markdown.push(
                `${key} ${requiredTag} | ${makeDetailsTag(`String`, attributes.join('<br>'), 1)?.replaceAll(
                  /\|/g,
                  '\\|'
                )}`
              )
            } else {
              markdown.push(`${key} ${requiredTag} | String`)
            }

            break
          }

          case 'integer': {
            Object.keys(component[key]).forEach(attr => {
              switch (attr) {
                case 'type': {
                  break
                }

                default: {
                  attributes.push(`${attr}: ${component[key][attr]}`)

                  break
                }
              }
            })

            if (attributes.length > 0) {
              markdown.push(
                `${key} ${requiredTag} | ${makeDetailsTag(`Integer`, attributes.join('<br>'), 1)?.replaceAll(
                  /\|/g,
                  '\\|'
                )}`
              )
            } else {
              markdown.push(`${key} ${requiredTag} | Integer`)
            }

            break
          }

          case 'number': {
            Object.keys(component[key]).forEach(attr => {
              switch (attr) {
                case 'type': {
                  break
                }

                default: {
                  attributes.push(`${attr}: ${component[key][attr]}`)

                  break
                }
              }
            })

            if (attributes.length > 0) {
              markdown.push(
                `${key} ${requiredTag} | ${makeDetailsTag(`Number`, attributes.join('<br>'), 1)?.replaceAll(
                  /\|/g,
                  '\\|'
                )}`
              )
            } else {
              markdown.push(`${key} ${requiredTag} | Number`)
            }

            break
          }

          case 'boolean': {
            Object.keys(component[key]).forEach(attr => {
              switch (attr) {
                case 'type': {
                  break
                }

                default: {
                  attributes.push(`${attr}: ${component[key][attr]}`)

                  break
                }
              }
            })

            if (attributes.length > 0) {
              markdown.push(
                `${key} ${requiredTag} | ${makeDetailsTag(`Boolean`, attributes.join('<br>'), 1)?.replaceAll(
                  /\|/g,
                  '\\|'
                )}`
              )
            } else {
              markdown.push(`${key} ${requiredTag} | Boolean`)
            }

            break
          }

          case 'array': {
            let shouldBreak = false;

            if (typeof component[key].items === 'object') {
              Object.keys(component[key].items).forEach(attr => {
                switch (attr) {
                  case '$ref': {
                    shouldBreak = true
                    const newRef = component[key].items['$ref'].slice(2)

                    markdown.push(
                      `${key} ${requiredTag} | ${makeDetailsTag(
                        `[${newRef.split('/').at(-1)}]`,
                        recursiveSchemaBuilder(newRef, 1),
                        1
                      )?.replaceAll(/\|/g, '\\|')}`
                    )

                    break
                  }

                  case 'type': {
                    attributes.push(`${attr}: ${component[key].items[attr]}`)

                    break
                  }

                  default: {
                    attributes.push(`${attr}: ${component[key].items[attr]}`)

                    break
                  }
                }
              })
            }

            if (shouldBreak) {
              break
            }

            markdown.push(`| ${key} | ${makeDetailsTag('Array', attributes.join('<br>'), 1)}`)

            break
          }

          case 'object': {
            markdown.push(...makeReqBodyTable(`${ref}/${key}`, required))

            break
          }

          default: {
            if (component[key]['$ref']) {
              const newRef = component[key]['$ref'].slice(2)
              markdown.push(
                `| ${key} ${requiredTag} | ${makeDetailsTag(`Object`, recursiveSchemaBuilder(newRef, 1), 1)?.replaceAll(
                  /\|/g,
                  '\\|'
                )} |`
              )
            }

            break
          }
        }

        break
      }
    }
  })

  return markdown
}

const recursiveSchemaBuilder = (ref, tabIndex, pathToCopy) => {
  const schema = []

  let component = doc

  let dirs = ref.split('/')

  let i
  let limit = dirs.length

  for (i = 0; i < limit; i++) {
    if (dirs[i].at(-1) === '~') {
      dirs = [...dirs.slice(0, i), dirs[i].slice(0, -1) + '/' + dirs[i + 1], ...dirs.slice(i + 2)]
      i -= 1
      limit--
    } else if (dirs[i].at(0) === '~') {
      dirs = [...dirs.slice(0, i - 1), dirs[i - 1] + '/' + dirs[i].slice(1), ...dirs.slice(i + 1)]
      i -= 2
      limit--
    }
  }

  dirs.forEach(key => {
    if (key.match(/\[([0-9]*)\]$/)) {
      const index = key.match(/\[([0-9]*)\]$/)[1]
      component = component[key.slice(0, -(index.length + 2))][parseInt(index)]
      if (component.schema) {
        component = component.schema
      }
    } else {
      component = component[key]
    }
  })

  let doDefault = false
  let defaultKey

  for (i = 0; i < Object.keys(component).length; i++) {
    const key = Object.keys(component)[i]
    const newPath = pathToCopy ? `${pathToCopy}['${key}']` : ''

    defaultKey = key

    const attributes = []

    if (ref.endsWith('Chapter')) {
      console.log('here')
    }

    switch (key) {
      case 'title': {
        if (dirs.at(-1) != 'properties') {
          doDefault = false
          break
        }

        if (component.title['$ref']) {
          const newRef = component.title['$ref'].slice(2)
          schema.push(
            makeDetailsTag(
              makeResponseItem(`title: ${newRef.split('/').at(-1)}`, newPath, newRef),
              recursiveSchemaBuilder(newRef, tabIndex, newPath),
              tabIndex + 1,
              `./static/components/schemas/${newRef.split('/').at(-1)}.html`
            )
          )

          doDefault = false

          break
        } else {
          doDefault = true
        }

        break
      }

      case 'type': {
        if (dirs.at(-1) != 'properties' || typeof component.type === 'undefined') {
          doDefault = false
          break
        }

        doDefault = true

        break
      }

      case 'properties': {
        schema.push(recursiveSchemaBuilder(`${ref}/${key}`, tabIndex, pathToCopy))

        doDefault = false

        break
      }

      case 'additionalProperties': {
        switch (component[key].type) {
          case 'object': {
            schema.push(
              makeDetailsTag(
                makeResponseItem('additionalProp1', `${newPath}['additionalProp1']`),
                recursiveSchemaBuilder(`${ref}/additionalProperties`, tabIndex, `${newPath}[additionalProp1]`),
                tabIndex + 1
              )
            )

            doDefault = false

            break
          }

          case 'string': {
            if (component[key].enum) {
              const enums = []

              component[key].enum.forEach(propEnum => {
                enums.push(`<p style="margin-bottom: 0">${propEnum}</p>`)
              })

              schema.push(
                makeDetailsTag(
                  makeResponseItem(`additionalProp1`, `${pathToCopy}['additionalProp1']`),
                  makeDetailsTag(
                    makeResponseItem(`Enum`, `${pathToCopy}['additionalProp1']`),
                    enums.join(''),
                    tabIndex
                  ),
                  tabIndex
                )
              )

              doDefault = false

              break
            }

            schema.push(makeResponseItem(`additionalProp1: String`, `${pathToCopy}['additionalProp1']`))

            doDefault = false

            break
          }

          default:
            break
        }

        doDefault = false

        break
      }

      case 'description': {
        if (dirs.at(-1) != 'properties') {
          attributes.push(`description: ${component.description}`)
          doDefault = false

          break
        }

        doDefault = true
        break
      }

      case 'x-examples': {
        doDefault = false
        break
      }

      default: {
        doDefault = true
        break
      }
    }

    if (doDefault) {
      if (defaultKey === 'properties') {
        return
      }

      let prop = component[defaultKey]
      const schemaItem = []

      if (typeof prop.nullable != 'undefined') {
        attributes.push(`nullable: ${prop.nullable}`)
      }

      if (prop.description) {
        attributes.push(`description: ${prop.description}`)
      }

      switch (prop.type) {
        case 'string': {
          Object.keys(prop).forEach(attr => {
            switch (attr) {
              case 'enum': {
                const enums = []

                prop.enum.forEach(propEnum => {
                  enums.push(`<p style="margin-bottom: 0">${propEnum}</p>`)
                })

                attributes.push(makeDetailsTag(makeResponseItem(`Enum`, newPath), enums.join(''), tabIndex + 1))

                break
              }

              case 'type': {
                break
              }

              case 'description': {
                break
              }

              case 'nullable': {
                break
              }

              default: {
                attributes.push(`${attr}: ${prop[attr]}`)
              }
            }
          })

          if (attributes.length > 0) {
            schema.push(
              makeDetailsTag(makeResponseItem(`${defaultKey}: String`, newPath), attributes.join('<br>'), tabIndex + 1)
            )
          } else {
            schema.push(makeResponseItem(`${defaultKey}: String`, newPath))
          }

          break
        }

        case 'array': {
          if (prop.items && prop.items['$ref']) {
            const newRef = prop.items['$ref'].slice(2)
            schemaItem.push(
              makeDetailsTag(
                makeResponseItem(`${defaultKey}: [${newRef.split('/').at(-1)}]`, newPath, newRef),
                recursiveSchemaBuilder(newRef, tabIndex, newPath),
                tabIndex + 1,
                `./static/components/schemas/${newRef.split('/').at(-1)}.html`
              )
            )

            schema.push(schemaItem.join(''))

            break
          } else if (prop.items) {
            Object.keys(prop.items).forEach(attr => {
              attributes.push(`${attr}: ${prop.items[attr]}`)
            })

            schema.push(
              makeDetailsTag(
                makeResponseItem(`${defaultKey}: Array`, `${newPath}[]`),
                attributes.join('<br>'),
                tabIndex + 1
              )
            )

            break
          } else {
            schema.push(makeResponseItem(`${key}: Array`, `${newPath}[]`))
          }

          break
        }

        case 'integer': {
          Object.keys(prop).forEach(attr => {
            switch (attr) {
              case 'type': {
                break
              }

              case 'description': {
                break
              }

              case 'nullable': {
                break
              }

              default: {
                attributes.push(`${attr}: ${prop[attr]}`)
              }
            }
          })

          if (attributes.length > 0) {
            schema.push(
              makeDetailsTag(makeResponseItem(`${defaultKey}: Integer`, newPath), attributes.join('<br>'), tabIndex + 1)
            )
          } else {
            schema.push(makeResponseItem(`${defaultKey}: Integer`, newPath))
          }

          break
        }

        case 'number': {
          Object.keys(prop).forEach(attr => {
            switch (attr) {
              case 'type': {
                break
              }

              case 'description': {
                break
              }

              case 'nullable': {
                break
              }

              default: {
                attributes.push(`${attr}: ${prop[attr]}`)
              }
            }
          })

          if (attributes.length > 0) {
            schema.push(
              makeDetailsTag(makeResponseItem(`${defaultKey}: Number`, newPath), attributes.join('<br>'), tabIndex)
            )
          } else {
            schema.push(makeResponseItem(`${defaultKey}: Number`, newPath))
          }

          break
        }

        case 'boolean': {
          schemaItem.push(`${defaultKey}: Boolean`)

          schema.push(makeResponseItem(schemaItem.join(''), newPath))

          break
        }

        case 'object': {
          if (prop.properties || prop.additionalProperties) {
            schema.push(
              makeDetailsTag(
                makeResponseItem(`${defaultKey}: Object`, newPath),
                recursiveSchemaBuilder(`${ref}/${defaultKey}`, tabIndex, newPath),
                tabIndex + 1
              )
            )
            break
          }

          schemaItem.push(`${defaultKey}: object`)

          schema.push(schemaItem.join(''))

          break
        }

        default: {
          if (prop['$ref']) {
            const newRef = prop['$ref'].slice(2)
            schema.push(
              makeDetailsTag(
                makeResponseItem(`${defaultKey}: ${newRef.split('/').at(-1)}`, newPath, newRef),
                recursiveSchemaBuilder(newRef, tabIndex , newPath),
                tabIndex + 1,
                `./static/components/schemas/${newRef.split('/').at(-1)}.html`
              )
            )
            break
          } else {
            schema.push(
              makeDetailsTag(
                makeResponseItem(`String`, newPath),
                `${prop}`,
                tabIndex + 1
              )
            )
          }

          break
        }
      }
    }
  }

  return schema.join('')
}

const makeExampleCodeTabs = (path, method) => {
  const markdown = []

  const items = doc.paths[path][method].responses

  Object.keys(items).forEach(status => {
    markdown.push(`+++ ${genStatusCode(status)}`)

    const schema = items[status].content['application/json'].schema

    if (schema.description) {
      markdown.push(schema.description)
    }

    const ref = schema['$ref']
      ? schema['$ref'].slice(2)
      : `paths/${path.replaceAll(/\//g, '~/')}/${method}/responses/${status}/content/application~/json/schema`

    markdown.push(recursiveSchemaBuilder(ref, 0, '$'))

    markdown.push('')
  })

  markdown.push('+++')

  return markdown
}

const generateMarkdown = (label, props, order) => {
  const markdown = []

  markdown.push('---')
  markdown.push('label: ' + label)
  markdown.push('order: ' + order)
  markdown.push('---')
  markdown.push('') // newline
  markdown.push('<!-- This file is auto-generated -->')
  markdown.push('')

  markdown.push('# ' + label)
  markdown.push('')

  props.forEach(prop => {
    const endpoint = doc.paths[prop.path][prop.method]
    const endpointParent = doc.paths[prop.path]

    const security = doc.paths[prop.path][prop.method].security

    markdown.push('## ' + endpoint.summary)

    if (typeof security === 'undefined' || security?.length > 0) {
      markdown.push(`Authorization: Bearer`)
    }

    markdown.push(
      `<p class="method${prop.method}">${prop.method.toUpperCase()}</p>` + makePathItem(prop.path.replace('{', '{'))
    )

    markdown.push('')

    markdown.push('')
    markdown.push(endpoint.description)
    markdown.push('')

    if (endpoint.parameters || endpointParent.parameters) {
      const methodPath = endpoint.parameters?.filter(target => target.in === 'path')
      const methodQuery = endpoint.parameters?.filter(target => target.in === 'query')
      const methodHeader = endpoint.parameters?.filter(target => target.in === 'header')

      const parentPath = endpointParent.parameters?.filter(target => target.in === 'path')
      const parentQuery = endpointParent.parameters?.filter(target => target.in === 'query')
      const parentHeader = endpointParent.parameters?.filter(target => target.in === 'header')

      const path = [...(methodPath ?? []), ...(parentPath ?? [])]
      const query = [...(methodQuery ?? []), ...(parentQuery ?? [])]
      const header = [...(methodHeader ?? []), ...(parentHeader ?? [])]

      if (path?.length > 0) {
        markdown.push('### Path parameters')
        markdown.push('')

        markdown.push('| Name | Type |')
        markdown.push('| ---- | ---- |')

        if (methodPath?.length > 0) {
          markdown.push(
            ...makePathTable(methodPath, `paths/${prop.path.replaceAll(/\//g, '~/')}/${prop.method}/parameters`)
          )
        } else if (parentPath?.length > 0) {
          markdown.push(...makePathTable(parentPath, `paths/${prop.path.replaceAll(/\//g, '~/')}/parameters`))
        }

        markdown.push('')
      }

      if (query?.length > 0) {
        markdown.push('### Query parameters')
        markdown.push('')

        markdown.push('| Name | Type |')
        markdown.push('| ---- | ---- |')

        if (methodQuery?.length > 0) {
          markdown.push(
            ...makeQueryTable(query, `paths/${prop.path.replaceAll(/\//g, '~/')}/${prop.method}/parameters`)
          )
        } else if (parentQuery?.length > 0) {
          markdown.push(...makeQueryTable(query, `paths/${prop.path.replaceAll(/\//g, '~/')}/parameters`))
        }

        markdown.push('')
      }

      if (header?.length > 0) {
        markdown.push('### Header parameters')
        markdown.push('')

        markdown.push('| Key | Value |')
        markdown.push('| --- | ----- |')

        markdown.push(...makeHeaderTable(header))
        markdown.push('')
      }
    }

    if (endpoint.requestBody) {
      Object.keys(endpoint.requestBody.content).forEach(contentType => {
        markdown.push('### Request body')
        markdown.push(endpoint.requestBody.description)
        markdown.push('')

        markdown.push('| Name | Value |')
        markdown.push('| ---- | ----- |')

        const ref = `paths/${prop.path.replaceAll(/\//g, '~/')}/${
          prop.method
        }/requestBody/content/${contentType.replaceAll(/\//g, '~/')}/schema`

        markdown.push(...makeReqBodyTable(ref, []))
        markdown.push('')
      })
    }

    if (endpoint.responses) {
      markdown.push('### Responses')
      markdown.push('`application/json`')
      markdown.push('')

      markdown.push(...makeExampleCodeTabs(prop.path, prop.method))
      markdown.push('')
    }
  })

  return markdown
}

try {
  const endpoints = []

  Object.keys(doc.paths).forEach(path => {
    Object.keys(doc.paths[path]).forEach(method => {
      if (['get', 'post', 'put', 'delete'].indexOf(method) === -1) {
        return
      }

      endpoints.push({
        path: path,
        method: method,
        tag: doc.paths[path][method].tags?.at(0)
      })
    })
  })

  const manga = endpoints.filter(endpoint => endpoint.path.startsWith('/manga'))
  const auth = endpoints.filter(endpoint => endpoint.path.startsWith('/auth/'))
  const account = endpoints.filter(endpoint => endpoint.path.startsWith('/account'))
  const group = endpoints.filter(endpoint => endpoint.path.startsWith('/group'))
  const list = endpoints.filter(endpoint => endpoint.path.startsWith('/list'))
  const user = endpoints.filter(endpoint => endpoint.path.startsWith('/user'))
  const chapter = endpoints.filter(endpoint => endpoint.path.startsWith('/chapter'))
  const cover = endpoints.filter(endpoint => endpoint.path.startsWith('/cover'))
  const author = endpoints.filter(endpoint => endpoint.path.startsWith('/author'))
  const legacy = endpoints.filter(endpoint => endpoint.path.startsWith('/legacy'))
  const captcha = endpoints.filter(endpoint => endpoint.path.startsWith('/captcha'))
  const report = endpoints.filter(endpoint => endpoint.path.startsWith('/report'))
  const upload = endpoints.filter(endpoint => endpoint.path.startsWith('/upload'))
  const rating = endpoints.filter(endpoint => endpoint.path.startsWith('/rating'))
  const statistics = endpoints.filter(endpoint => endpoint.path.startsWith('/statistics'))
  const settings = endpoints.filter(endpoint => endpoint.path.startsWith('/settings'))
  const infrastructure = endpoints.filter(endpoint => endpoint.path.startsWith('/ping'))

  let i = endpoints.length

  fs.mkdirSync('.tmp', { recursive: true })
  fs.mkdirSync('./static/components/schemas', { recursive: true })

  let markdown

  markdown = generateMarkdown('Manga', manga, i--)
  fs.writeFileSync(`.tmp/Manga.md`, markdown.join('\n'))

  markdown = generateMarkdown('Auth', auth, i--)
  fs.writeFileSync(`.tmp/Auth.md`, markdown.join('\n'))

  markdown = generateMarkdown('Account', account, i--)
  fs.writeFileSync(`.tmp/Account.md`, markdown.join('\n'))

  markdown = generateMarkdown('Group', group, i--)
  fs.writeFileSync(`.tmp/Group.md`, markdown.join('\n'))

  markdown = generateMarkdown('List', list, i--)
  fs.writeFileSync(`.tmp/List.md`, markdown.join('\n'))

  markdown = generateMarkdown('User', user, i--)
  fs.writeFileSync(`.tmp/User.md`, markdown.join('\n'))

  markdown = generateMarkdown('Chapter', chapter, i--)
  fs.writeFileSync(`.tmp/Chapter.md`, markdown.join('\n'))

  markdown = generateMarkdown('Cover', cover, i--)
  fs.writeFileSync(`.tmp/Cover.md`, markdown.join('\n'))

  markdown = generateMarkdown('Author', author, i--)
  fs.writeFileSync(`.tmp/Author.md`, markdown.join('\n'))

  markdown = generateMarkdown('Legacy', legacy, i--)
  fs.writeFileSync(`.tmp/Legacy.md`, markdown.join('\n'))

  markdown = generateMarkdown('Captcha', captcha, i--)
  fs.writeFileSync(`.tmp/Captcha.md`, markdown.join('\n'))

  markdown = generateMarkdown('Report', report, i--)
  fs.writeFileSync(`.tmp/Report.md`, markdown.join('\n'))

  markdown = generateMarkdown('Upload', upload, i--)
  fs.writeFileSync(`.tmp/Upload.md`, markdown.join('\n'))

  markdown = generateMarkdown('Rating', rating, i--)
  fs.writeFileSync(`.tmp/Rating.md`, markdown.join('\n'))

  markdown = generateMarkdown('Statistics', statistics, i--)
  fs.writeFileSync(`.tmp/Statistics.md`, markdown.join('\n'))

  markdown = generateMarkdown('Settings', settings, i--)
  fs.writeFileSync(`.tmp/Settings.md`, markdown.join('\n'))

  markdown = generateMarkdown('Infrastructure', infrastructure, i--)
  fs.writeFileSync(`.tmp/Infrastructure.md`, markdown.join('\n'))
} catch (err) {
  console.error(err)
}
