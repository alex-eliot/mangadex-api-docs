---
order: 81
---
# Authentication

You can login with the `/auth/login` endpoint. On success, it will return a JWT that remains valid for 15 minutes along with a session token that allows refreshing without re-authenticating for 1 month.

## Security issues

If you believe you found a security issue in our API, please check our [security.txt](https://api.mangadex.org/security.txt) to get in touch privately.
