const { createHash } = require('crypto')
const { execSync } = require('child_process')
const fs = require('fs')

try {
    if (fs.readdirSync('./').indexOf('docs') != -1) {
        const hashes = []
        fs.readdirSync('./docs').forEach(md => {
            hashes.push(createHash('sha256').update(fs.readFileSync(`./docs/${md}`).toString()).digest('hex'))
        })
    
        execSync('node generator.js')
    
        const newHashes = []
        fs.readdirSync('./.tmp').forEach(md => {
            newHashes.push(createHash('sha256').update(fs.readFileSync(`./.tmp/${md}`).toString()).digest('hex'))
        })
    
        if (hashes.join('') != newHashes.join('')) {
            console.log('File change detected!')
            fs.rmSync(`./docs`, { recursive: true, force: true })
            fs.renameSync('.tmp', 'docs')
        } else {
            console.log('Nothing to do!')
            fs.rmSync(`.tmp`, { recursive: true, force: true })
        }
    } else {
        execSync('node generator.js')
        fs.renameSync('.tmp', 'docs')
    }
} catch (err) {
    console.log(err)
}